note
	description: "Summary description for {PROPOSITION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROPOSITION

inherit
	DIA_TEXT_BOX
		rename
			make as make_text_box
		redefine
			default_create
		end
	PREMISE
		redefine
			default_create
		end
create
	make

feature {NONE} -- Initialization

	default_create
			-- Initialization of `Current'
		do
			Precursor {DIA_TEXT_BOX}
			Precursor {PREMISE}
		end

	make(a_diagram:ARGUMENT_DIAGRAM;a_text:READABLE_STRING_GENERAL)
			-- Initialization of `Current' using `a_diagram' as `diagram' and `a_text' as `text'.`text'
		do
			make_text_box("sans 12")
			set_diagram (a_diagram)
			set_width(200)
			text.set_text (a_text)
		ensure
			Diagram_Valid: diagram ~ a_diagram
			Text_Valid: text.text ~ a_text
		end

invariant
	Diagram_Is_Argument: attached diagram implies attached {ARGUMENT_DIAGRAM} diagram

end
