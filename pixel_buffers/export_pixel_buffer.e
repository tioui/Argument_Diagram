note
	description: "Pixel buffer that replaces original image file.%
		%The original version of this class has been generated by Image Eiffel Code."

class
	EXPORT_PIXEL_BUFFER

inherit
	ABSTRACT_PIXEL_BUFFER

create
	make

feature {NONE} -- Image data

	Image_width:INTEGER = 16
			-- <Precursor>

	Image_height:INTEGER = 16
			-- <Precursor>

	c_colors_0 (a_ptr: POINTER; a_offset: INTEGER)
			-- Fill `a_ptr' with colors data from `a_offset'.
		external
			"C inline"
		alias
			"{
			{
				#define B(q) \
					#q
				#ifdef EIF_WINDOWS
				#define A(a,r,g,b) \
					B(\x##b\x##g\x##r\x##a)
				#else
				#define A(a,r,g,b) \
					B(\x##r\x##g\x##b\x##a)
				#endif
				char l_data[] = 
				A(00,00,00,00)A(77,C1,C3,EE)A(FF,AE,B0,E5)A(EB,9C,A1,E1)A(EA,8A,8F,D9)A(EA,7A,7C,D0)A(EA,6A,6B,CB)A(EA,56,58,C3)A(EA,46,48,BA)A(EA,33,33,B5)A(EA,23,24,AD)A(EA,10,10,A7)A(EA,10,10,A7)A(F8,01,01,9C)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(7B,B0,B4,E6)A(EF,A8,AA,E5)A(FF,B3,B7,F5)A(FF,A7,AA,F3)A(FF,9F,A3,F1)A(FF,99,9C,EF)A(FF,91,93,ED)A(FF,8B,8C,EB)A(FF,83,84,EA)A(FF,7C,7D,E8)A(FF,80,80,E8)A(EC,0F,0F,A5)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(78,A8,AA,DF)A(EF,9B,9E,E0)A(FF,AA,AF,F3)A(FF,9B,9E,F1)A(FF,93,96,EF)A(FF,8C,8E,ED)A(FF,84,86,EB)A(FF,7C,7D,E8)A(FF,75,75,E6)A(FF,7A,7A,E7)A(EC,0F,0F,A5)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(A9,9A,9A,DE)A(FF,A2,A4,EA)A(FF,9E,A2,F2)A(FF,95,99,EF)A(FF,8E,90,ED)A(FF,86,88,EB)A(FF,7F,80,E9)A(FF,77,78,E7)A(FF,7A,7A,E7)A(EB,0F,0F,A5)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(DF,A0,A5,E0)A(FF,AE,B3,F4)A(FF,9F,A3,F2)A(FF,98,9B,F0)A(FF,90,93,EE)A(FF,89,8B,EC)A(FF,81,83,EA)A(FF,79,7A,E8)A(FF,7C,7C,E8)A(EB,0F,0F,A5)A(00,00,00,00)A(00,00,00,00)
				A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(2D,BB,BB,E3)A(EB,AA,AE,E7)A(FF,AE,B3,F6)A(FF,A2,A6,F3)A(FF,9A,9E,F1)A(FF,93,96,EF)A(FF,8B,8D,ED)A(FF,83,85,EA)A(FF,7C,7D,E8)A(FF,7E,7F,E8)A(EB,0F,0F,A5)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(93,BB,BF,EC)A(FE,B7,BB,F2)A(FF,AD,B2,F6)A(FF,A4,A9,F4)A(FF,9D,A0,F1)A(FF,95,98,EF)A(FF,8D,90,ED)A(FF,87,89,EB)A(FF,7E,7F,E9)A(FF,81,81,E9)A(EB,0F,0F,A5)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(E2,C3,C7,EE)A(FF,BD,C2,F8)A(FF,AE,B3,F6)A(FF,A7,AB,F4)A(FF,A0,A4,F2)A(FF,A0,A4,F0)A(FE,85,86,DF)A(FF,87,89,E5)A(FF,83,84,EA)A(FF,82,83,EA)A(EB,14,14,A6)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(10,CF,CF,FF)A(EA,CA,CD,F2)A(FF,BD,C2,F9)A(FF,B0,B6,F7)A(FF,AC,B1,F5)A(FE,A2,A6,ED)A(EA,6F,71,CD)A(96,5C,5C,C4)A(E4,4B,4B,BC)A(FF,7E,80,E1)A(FF,87,88,EA)A(EB,19,19,A8)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(22,E1,E9,F8)A(EA,CE,D0,F4)A(FF,BD,C4,FA)A(FF,B4,BA,F8)A(FE,AE,B3,F1)A(DC,88,88,D4)A(21,64,74,C9)A(00,00,00,00)A(23,49,49,B6)A(E4,3F,3F,B9)A(FF,7A,7A,DF)A(EB,1D,1D,AD)A(00,00,00,00)A(00,00,00,00)
				A(00,00,00,00)A(00,00,00,00)A(16,E8,E8,FF)A(EA,D2,D7,F7)A(FF,C1,C7,FA)A(FF,BC,C1,F9)A(E9,A0,A2,E3)A(1D,8D,8D,CA)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(1E,3C,3C,B3)A(E3,30,30,B3)A(F5,1B,1B,A9)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(DB,DB,DD,F8)A(FF,C6,CC,FA)A(FF,BF,C3,F7)A(B3,A4,A7,E3)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(1D,2C,2C,B0)A(E1,1C,1C,A9)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(73,E0,E7,FB)A(F4,CB,CF,F7)A(FF,C0,C4,F5)A(80,A7,A9,E3)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(1C,1B,1B,A4)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(03,FF,FF,FF)A(D9,D4,D6,F6)A(FF,C2,C7,F3)A(85,AE,B0,E8)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(21,D1,D8,F0)A(EC,C8,CA,F1)A(C9,B3,B8,E8)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)
				A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(29,C7,CD,F3)A(AF,BC,BC,EB)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00)A(00,00,00,00);
				memcpy ((EIF_NATURAL_32 *)$a_ptr + $a_offset, &l_data, sizeof l_data - 1);
			}
			}"
		end

	build_colors (a_ptr: POINTER)
			-- <Precursor>
		do
			c_colors_0 (a_ptr, 0)
		end

end -- EXPORT_PIXEL_BUFFER
