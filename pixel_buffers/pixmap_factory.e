note
	description: "Summary description for {PIXMAP_FACTORY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PIXMAP_FACTORY


feature -- Access

	Cancel_pixmap:EV_PIXMAP
			-- The "Cancel" icon
		once
			create Result.make_with_pixel_buffer(create {CANCEL_PIXEL_BUFFER}.make)
		end

	Export_pixmap:EV_PIXMAP
			-- The "export" icon
		once
			create Result.make_with_pixel_buffer(create {EXPORT_PIXEL_BUFFER}.make)
		end

	New_pixmap:EV_PIXMAP
			-- The "new file" icon
		once
			create Result.make_with_pixel_buffer(create {NEW_PIXEL_BUFFER}.make)
		end

	Ok_pixmap:EV_PIXMAP
			-- The "Ok" icon
		once
			create Result.make_with_pixel_buffer(create {OK_PIXEL_BUFFER}.make)
		end

	Open_pixmap:EV_PIXMAP
			-- The "open file" icon
		once
			create Result.make_with_pixel_buffer(create {OPEN_PIXEL_BUFFER}.make)
		end

	Save_pixmap:EV_PIXMAP
			-- The "save file" icon
		once
			create Result.make_with_pixel_buffer(create {SAVE_PIXEL_BUFFER}.make)
		end

	Save_as_pixmap:EV_PIXMAP
			-- The "save as file" icon
		once
			create Result.make_with_pixel_buffer(create {SAVE_AS_PIXEL_BUFFER}.make)
		end

end
