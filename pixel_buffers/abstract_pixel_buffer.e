note
	description: "Summary description for {ABSTRACT_PIXEL_BUFFER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	ABSTRACT_PIXEL_BUFFER

inherit
	EV_PIXEL_BUFFER

feature {NONE} -- Initialization

	make
			-- Initialization
		do
			make_with_size (Image_width, Image_height)
			fill_memory
		end

feature {NONE} -- Image data

	Image_width:INTEGER
			-- Horizontal dimension of `Current'
		deferred
		end

	Image_height:INTEGER
			-- Vertical dimension od `Current'
		deferred
		end

	build_colors (a_ptr: POINTER)
			-- Build `colors'.
		deferred
		end

feature {NONE} -- Image data filling.

	fill_memory
			-- Fill image data into memory.
		local
			l_pointer: POINTER
		do
			if attached {EV_PIXEL_BUFFER_IMP} implementation as l_imp then
				l_pointer := l_imp.data_ptr
				if not l_pointer.is_default_pointer then
					build_colors (l_pointer)
					l_imp.unlock
				end
			end
		end

end
