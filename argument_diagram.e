note
	description: "Summary description for {ARGUMENT_DIAGRAM}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ARGUMENT_DIAGRAM

inherit
	DIA_DIAGRAM
		rename
			width as context_width,
			height as cotext_height,
			set_width as set_context_width,
			set_height as set_context_height
		export
			{NONE} elements, add_element, remove_element, links, add_link, remove_link, move_to_top
		end


create
	make_from_image_surface

feature -- Access

	can_set_proposition:BOOLEAN
			-- `set_top_proposition' can be used
		do
			Result := not attached top_proposition
		end

	can_add_premise:BOOLEAN
			-- `add_premise'can be used
		do
			Result := attached selected_proposition
		end

	can_set_conclusion:BOOLEAN
			-- `set_conclusion'can be used
		do
			Result := attached selected_proposition
		end

	can_link_premise:BOOLEAN
			-- `link_premise'can be used
		do
			Result := attached selected_proposition and selected_proposition /~ top_proposition
		end

	top_proposition:detachable PROPOSITION
			-- The {PROPOSITION} at the top of `Current'

	set_top_proposition(a_proposition:PROPOSITION)
			-- Assign `top_proposition' with the value of `a_proposition'
		require
			Can_Set: can_set_proposition
		do
			add_element (a_proposition)
			top_proposition := a_proposition
			selected_proposition := a_proposition
			a_proposition.set_stroke_color (0.0, 0.0, 1.0, 1.0)
			replace_propositions_position
		ensure
			Is_Assign: attached top_proposition as la_proposition and then la_proposition ~ a_proposition
		end

	add_premise(a_proposition:PROPOSITION)
			-- Add the premise `a_proposition' to `selected_proposition'
		require
			Can_Add_Premise: can_add_premise
		do
			if attached selected_proposition as la_proposition then
				add_element (a_proposition)
				add_conclusion_to_premise(a_proposition, la_proposition)
			end
		ensure
			Premise_Added: attached selected_proposition as la_proposition implies la_proposition.premises.has(a_proposition)
			Conclusion_Valid: a_proposition.conclusion ~ selected_proposition
			Selected_Proposition_Not_Changed: selected_proposition ~ selected_proposition
		end

	set_concluson(a_proposition:PROPOSITION)
			-- Assign the conclusion `a_proposition' to `selected_proposition'
		require
			Can_Add_Premise: can_set_conclusion
		do
			if attached selected_proposition as la_proposition then
				add_element (a_proposition)
				add_conclusion_to_premise(la_proposition, a_proposition)
			end
		ensure
			Premise_Added: attached selected_proposition as la_proposition implies la_proposition.premises.has(a_proposition)
			Conclusion_Valid: a_proposition.conclusion ~ selected_proposition
			Selected_Proposition_Not_Changed: selected_proposition ~ selected_proposition
		end

	selected_proposition:detachable PROPOSITION
			-- The {PROPOSITION} of `Current' the the user has selected

	on_mouse_down(a_x, a_y:INTEGER)
			-- The user pressed on the mouse at `a_x', `a_y'
		do
			mouse_down_x := a_x
			mouse_down_y := a_y
		ensure
			Is_Set: mouse_down_x ~ a_x and mouse_down_y ~ a_y
		end

	on_mouse_up(a_x, a_y:INTEGER)
			-- The user released on the mouse at `a_x', `a_y'
		do
			if attached selected_proposition as la_argument then
				la_argument.set_stroke_color (0.0, 0.0, 0.0, 1.0)
			end
			selected_proposition := Void
			from
				elements_external.start
			until
				elements_external.exhausted or
				attached selected_proposition
			loop
				if
					attached {PROPOSITION} elements_external.item as la_proposition and then (
					mouse_down_x > la_proposition.x and mouse_down_x < la_proposition.x + la_proposition.width and
					mouse_down_y > la_proposition.y and mouse_down_y < la_proposition.y + la_proposition.height and
					a_x > la_proposition.x and a_x < la_proposition.x + la_proposition.width and
					a_y > la_proposition.y and a_y < la_proposition.y + la_proposition.height )

				then
					selected_proposition := la_proposition
				end
				elements_external.forth
			end
			if attached selected_proposition as la_proposition then
				la_proposition.set_stroke_color (0.0, 0.0, 1.0, 1.0)
			end
		end

	width:INTEGER
			-- Horizontal dimension of `Current' while taking into consideration every `elements' ans `links'
		local
			l_demi_size:INTEGER
		do
			Result := internal_width
			across
				elements_external as la_elements
			loop
				if la_elements.item.x + la_elements.item.width + 10 > Result then
					Result := la_elements.item.x + la_elements.item.width
				end
			end
			across
				links_external as la_links
			loop
				l_demi_size := (la_links.item.stroke_size // 2)
				if la_links.item.start_position.x + l_demi_size + 10 > Result then
					Result := la_links.item.start_position.x + l_demi_size
				end
				if la_links.item.end_position.x + l_demi_size + 10 > Result then
					Result := la_links.item.end_position.x + l_demi_size
				end
				across
					la_links.item.corner_anchors as la_corner
				loop
					if la_corner.item.x + l_demi_size + 10 > Result then
						Result := la_corner.item.x + l_demi_size
					end
				end
			end
		end

	height:INTEGER
			-- Vertical dimension of `Current' while taking into consideration every `elements' ans `links'
		local
			l_demi_size:INTEGER
		do
			Result := internal_height
			across
				elements_external as la_elements
			loop
				if
					attached {PROPOSITION} la_elements.item as la_argument and then
					la_argument.y + la_argument.height + 10 > Result
				then
					Result := la_argument.y + la_argument.height
				end
			end
			across
				links_external as la_links
			loop
				l_demi_size := (la_links.item.stroke_size // 2)
				if la_links.item.start_position.y + l_demi_size + 10 > Result then
					Result := la_links.item.start_position.y + l_demi_size
				end
			end
		end

feature {NONE} -- Implementation

	add_conclusion_to_premise(a_premise:PREMISE; a_conclusion:PROPOSITION)
			-- Add `a_conclusion' as the conclusion of `a_premise'
		local
			l_link:DIA_LINK
			l_marker:DIA_ARROW_MARKER
		do
			a_premise.set_conclusion (a_conclusion)
			replace_propositions_position
			if attached {DIA_ANCHOR} a_premise as la_anchor then
				create l_link.make (la_anchor, a_conclusion)
				add_link (l_link)
				create l_marker.make (10, 20)
				l_link.set_end_marker (l_marker)
			end
		ensure
			Premise_Added: attached a_conclusion as la_proposition implies la_proposition.premises.has(a_premise)
			Conclusion_Valid: a_premise.conclusion ~ a_conclusion
		end

	replace_propositions_position
			-- Replace every propositions of `Current'
		local
			l_dimensions:TUPLE[lines_width, lines_height:LINKED_LIST[INTEGER]]
			l_lines_x:ARRAYED_LIST[INTEGER]
			l_max_width, l_demi_max_width:INTEGER
		do
			if attached top_proposition as la_proposition then
				l_dimensions := lines_dimension(la_proposition)
				l_max_width := 0
				across l_dimensions.lines_width as la_lines_width loop
					if la_lines_width.item > l_max_width then
						l_max_width := la_lines_width.item
					end
				end
				l_demi_max_width := (l_max_width // 2) + Left_border_size
				create l_lines_x.make (l_dimensions.lines_height.count)
				across l_dimensions.lines_width as la_lines_width loop
					l_lines_x.extend (l_demi_max_width - (la_lines_width.item // 2))
				end
				set_positions(la_proposition, l_dimensions.lines_height, l_lines_x, 1, Top_border_size)
			end
		end

	lines_dimension(a_premise:PREMISE):TUPLE[lines_width, lines_height:LINKED_LIST[INTEGER]]
			-- Get the width (including `Width_border_size') and height (excluding `Height_border_size')
			-- of every lines below `a_premise'
		local
			l_next_dimensions:TUPLE[lines_width, lines_height:LINKED_LIST[INTEGER]]
		do
			Result := [create {LINKED_LIST[INTEGER]}.make, create {LINKED_LIST[INTEGER]}.make]
			Result.lines_width.extend (a_premise.width + Width_border_size)
			Result.lines_height.extend (a_premise.height)
			across a_premise.premises as la_premise loop
				l_next_dimensions := lines_dimension(la_premise.item)
				adjust_dimensions(l_next_dimensions.lines_width, Result.lines_width, agent (i, j:INTEGER):INTEGER do Result := i + j end)
				adjust_dimensions(l_next_dimensions.lines_height, Result.lines_height, agent (i, j:INTEGER):INTEGER do Result := i.max (j) end)
			end
		ensure
			Lines_Count_Valid: Result.lines_width.count ~ Result.lines_height.count
		end

	adjust_dimensions(a_lines_dimensions_source, a_lines_dimensions_destination:LINKED_LIST[INTEGER];
						a_operation:FUNCTION[INTEGER, INTEGER, INTEGER])
			-- Adjust every `a_lines_dimensions_destination' using the `a_operation' on
			-- the every elements of `a_lines_dimensions_destination' and of the respective
			-- element of `a_lines_dimensions_source'.
			-- If `a_lines_dimensions_source' is bigger than `a_lines_dimensions_destination',
			-- copy every following elements of `a_lines_dimensions_source' in `a_lines_dimensions_destination'
		do
			from
				a_lines_dimensions_source.start
				a_lines_dimensions_destination.start
				a_lines_dimensions_destination.forth
			until
				a_lines_dimensions_source.exhausted or
				a_lines_dimensions_destination.exhausted
			loop
				a_lines_dimensions_destination.put(a_operation(a_lines_dimensions_destination.item,a_lines_dimensions_source.item))
				a_lines_dimensions_source.forth
				a_lines_dimensions_destination.forth
			end
			if a_lines_dimensions_destination.exhausted and not a_lines_dimensions_source.exhausted then
				from until a_lines_dimensions_source.exhausted loop
					a_lines_dimensions_destination.extend(a_lines_dimensions_source.item)
					a_lines_dimensions_source.forth
				end
			end
		ensure
			Lines_Count_Valid: a_lines_dimensions_destination.count >= a_lines_dimensions_source.count
		end

	set_positions(a_premise:PREMISE; a_lines_height, a_lines_x:LIST[INTEGER]; a_line, a_y:INTEGER)
			-- Adjust the `x' of `a_premise' considering that `a_premise' is at the line `a_line'
			-- and that `a_line_x' hold every lines of `current'. Also set the `x' of `a_premise'
			-- to `a_y'. Recurse on every `a_premise'.`premises'.
		require
			Lines_Count_Valid: a_lines_x.count ~ a_lines_height.count
		do
			if attached {PROPOSITION} a_premise as la_proposition then
				la_proposition.set_x (a_lines_x.at (a_line))
				a_lines_x.at (a_line) := a_lines_x.at (a_line) + Width_border_size + la_proposition.width
				la_proposition.set_y (a_y)
			end
			across a_premise.premises as la_premises loop
				set_positions(la_premises.item, a_lines_height, a_lines_x, a_line + 1, a_y + a_lines_height.at (a_line) + Height_border_size)
			end
		end

	mouse_down_x:INTEGER
			-- The horizontal coordinate of the last `on_mouse_down'

	mouse_down_y:INTEGER
			-- The vertical coordinate of the last `on_mouse_down'

feature {NONE} -- Constants

	Width_border_size:INTEGER = 10
			-- The horizontal space between two propositions

	Top_border_size:INTEGER = 10
			-- The horizontal space between the top of `Current' and the `top_proposition'

	Height_border_size:INTEGER = 100
			-- The vertical space between two propositions

	Left_border_size:INTEGER = 10
			-- The vertical space between the left of `Current' and the left most {PROPOSITION}

end
