note
	description: "Summary description for {PREMISE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PREMISE

inherit
	ANY
		redefine
			default_create
		end

feature {NONE} -- Initialization

	default_create
			-- <Precursor>
		do
			create {LINKED_LIST[PREMISE]} internal_premises.make
		end

feature -- Access

	width:INTEGER
			-- Horizontal dimension of `Current'
		deferred
		end

	height:INTEGER
			-- Vertical dimension of `Current'
		deferred
		end

	premises:LIST[PREMISE]
			-- Every {PREMISE} that precede `Current'
		do
			create {ARRAYED_LIST[PREMISE]} Result.make(internal_premises.count)
			Result.append (internal_premises)
		end

	conclusion:detachable PROPOSITION
			-- The conclusion that `Current' precise

feature {ARGUMENT_DIAGRAM}

	set_conclusion(a_conclusion:detachable PROPOSITION)
			-- Assign `conclusion' with the value of `a_conclusion'
		do
			if attached conclusion as la_conclusion then
				la_conclusion.remove_premise (Current)
			end
			conclusion := a_conclusion
			if attached conclusion as la_conclusion then
				la_conclusion.add_premise(Current)
			end

		end

feature {PREMISE} -- Implementation

	remove_premise(a_premise:PREMISE)
			-- Remove `a_premise' to `premises'
		require
			Is_Inserted: premises.has (a_premise)
		do
			internal_premises.prune_all (a_premise)
		ensure
			Is_Not_Inserted: not premises.has (a_premise)
		end

	add_premise(a_premise:PREMISE)
			-- Add `a_premise' to `premises'
		require
			Not_Already_Inserted: not premises.has (a_premise)
		do
			internal_premises.extend (a_premise)
		ensure
			Is_Inserted: premises.has (a_premise)
		end

feature {NONE} -- Implementation

	internal_premises:LIST[PREMISE]
			-- Internal representation of `premises'

invariant
	Conclusion_Valid: attached conclusion as la_conclusion implies la_conclusion.premises.has (Current)
	Premises_Valid: across premises as la_premises all attached la_premises.item.conclusion as la_conclusion and then la_conclusion ~ Current end
end
